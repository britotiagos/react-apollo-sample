import React, { useState } from "react";
import PostsList from "./PostsList";

function PostsSearch() {
	const [search, setSearch] = useState("");

	return (
		<div className="posts-search">
			<form
				className="posts-search__form"
				onSubmit={e => {
					e.preventDefault();
				}}
			>
				<input
					type="text"
					className="posts-search__search-field"
					name="searchQuery"
					value={search}
					onChange={e => setSearch(e.target.value)}
					placeholder="Search for blog posts…"
				/>
			</form>
			{search && (
				<div className="results">
					<PostsList searchQuery={search} />
				</div>
			)}
		</div>
	);
}

export default PostsSearch;
